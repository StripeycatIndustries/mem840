mem840 is a frequency memory management tool for the Yaesu FT-840 HF radio. 
Licenced under GPL. USE AT YOUR OWN RISK.

mem840 transfers the radio's entire 100 memories to/from an xlsx spreadsheet for editing.

Download frequencies from spreadsheet to radio:  python3 ft840mem.py -d filename.xlsx  

Upload frequencies from radio to spreadsheet:    python3 ft840mem.py -u filename.xlsx  

Use the upload option to generate initial spreadsheet, then edit as approriate and download
