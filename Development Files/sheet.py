import openpyxl

mem_wb=openpyxl.Workbook()
mem_ws = mem_wb.active

mem_ws.cell(1,1,"Memory")
mem_ws.cell(1,2,"Freq A")
mem_ws.cell(1,3,"Mode A")
mem_ws.cell(1,4,"Freq B")
mem_ws.cell(1,5,"Mode B")

mem_ws.cell(2,2,5680000)
mem_ws.cell(2,3,"USB")

for mem_num in range (1,101):
  mem_ws.cell(mem_num+1,1,mem_num)

mem_wb.save('test2.xlsx')
