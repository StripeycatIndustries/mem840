#Yaesu FT-840 memory management application

import serial
import sys
import time
import openpyxl

SPLIT        = 0x01
VFO_TO_MEM   = 0x03
SELECT_VFO   = 0x05
SET_VFO_FREQ = 0x0A
SET_MODE     = 0x0C
LSB          = 0
USB          = 1
CW           = 2
AM           = 4
FM           = 6
VFO_A        = 0
VFO_B        = 1

def set_freq(freq,serial_port):
  freq_hz = int(freq * 1e6)
  _10_100mhz   = int(freq_hz / 1e7)
  _1mhz        = int((freq_hz % 1e7) / 1e6)
  _100khz      = int((freq_hz % 1e6) / 1e5)
  _1mhz_100khz = (_1mhz * 0x10) + _100khz
  _10khz       = int((freq_hz % 1e5) / 1e4)
  _1khz        = int((freq_hz % 1e4) / 1e3)
  _10_1khz     = (_10khz * 0x10) + _1khz
  _100hz       = int((freq_hz % 1e3) / 1e2)
  _10hz        = int((freq_hz % 1e2) / 10)
  _100_10hz    = (_100hz * 0x10) + _10hz
  #print(hex(_10_100mhz), hex(_1mhz_100khz), hex(_10_1khz), hex(_100_10hz)) 
  #print( bytes(str(_10_100mhz),'latin1'), bytes(str(_1mhz_100khz),'latin1'),   bytes(str(_10_1khz),'latin1'), bytes(str(_100_10hz),'latin1') )
  serial_port.write( [_100_10hz, _10_1khz, _1mhz_100khz, _10_100mhz, SET_VFO_FREQ] )

def sel_vfo_a(serial_port):
  serial_port.write( [0, 0, 0, 0, SELECT_VFO] )
  
def sel_vfo_b(serial_port):
  serial_port.write( [0, 0, 0, 1, SELECT_VFO] )

def vfo_to_mem(serial_port, memory):
  serial_port.write( [0, 0, 0, memory, VFO_TO_MEM] )

def split_on(serial_port):
  serial_port.write( [0, 0, 0, 1, SPLIT] )

def split_off(serial_port):
  serial_port.write( [0, 0, 0, 0, SPLIT] )


ser = serial.Serial('/dev/ttyUSB0', baudrate=4800, bytesize=8, parity='N', stopbits=2)  #TIMEOUT

freq_a_str=input("VFO A: ")
#freq_b_str=input("VFO B: ")
freq_a_float = float(freq_a_str)

#freq_a_hz = int(freq_a_float * 1e6)

sel_vfo_a(ser)
set_freq(freq_a_float, ser)

freq_b_str=input("VFO B: ")
freq_b_float = float(freq_b_str)
sel_vfo_b(ser)
set_freq(freq_b_float, ser)

sel_vfo_a(ser)
split_on(ser)
vfo_to_mem(ser, 60)

#tx_data = set_freq(freq_a_float)

#tx_data=[bytes(str(_100_10hz),'latin1'), bytes(str(_10_1khz),'latin1'), bytes(str(_1mhz_100khz),'latin1'), bytes(str(_10_100mhz),'latin1'), SET_VFO_FREQ]

#tx_data=[_100_10hz, _10_1khz, _1mhz_100khz, _10_100mhz, SET_VFO_FREQ]

#tx_data=[0x00,0x50,0x42,0x01,0x0A]
#tx_data=[0x00,0x00,0x00,0x00,0x0A]
#print("tx_data: ", tx_data[0], tx_data[1], tx_data[2], tx_data[3], tx_data[4] )

#ser.write(tx_data)


#tx_data=[0x00,0x00,0x00,VFO_A,SELECT_VFO]
#ser.write(tx_data)
#tx_data=[0x00,0x50,0x42,0x01,0x0A] #SET FREQ 14.250
#ser.write(tx_data)
#tx_data=[0x00,0x00,0x00,VFO_B,SELECT_VFO]
#ser.write(tx_data)
#tx_data=[0x00,0x50,0x42,0x01,0x0A] #SET FREQ 14.250
#ser.write(tx_data)

ser.close


#def set_vfo_freq(frequency_mhz):
#  frequency_hz = int(freqency_mhz*1e6)

#
 











  


