# Yaesu FT-840 memory management application
# Released under GNU Public Licence
# Experimental software - use at your own risk
# Jim Beeley GM0UIN Jul 2020
# www.stripeycatindustries.com

#ADD: support splits, check valid data in spreadsheet

import serial
import os
import sys
#import time
import openpyxl
import argparse

ACQ_ALL_DATA=[0x00,0x00,0x00,0x00,0x10]

SPLIT        = 0x01
VFO_TO_MEM   = 0x03
SELECT_VFO   = 0x05
SET_VFO_FREQ = 0x0A
SET_MODE     = 0x0C
LSB          = 0
USB          = 1
CW           = 2
AM           = 4
FM           = 6
VFO_A        = 0
VFO_B        = 1

def set_freq(freq, serial_port):
  freq_hz = int(freq * 1e6)
  _10_100mhz   = int(freq_hz / 1e7)
  _1mhz        = int((freq_hz % 1e7) / 1e6)
  _100khz      = int((freq_hz % 1e6) / 1e5)
  _1mhz_100khz = (_1mhz * 0x10) + _100khz
  _10khz       = int((freq_hz % 1e5) / 1e4)
  _1khz        = int((freq_hz % 1e4) / 1e3)
  _10_1khz     = (_10khz * 0x10) + _1khz
  _100hz       = int((freq_hz % 1e3) / 1e2)
  _10hz        = int((freq_hz % 1e2) / 10)
  _100_10hz    = (_100hz * 0x10) + _10hz
  serial_port.write( [_100_10hz, _10_1khz, _1mhz_100khz, _10_100mhz, SET_VFO_FREQ] )


VFO_TO_MEM

def sel_vfo_a(serial_port):
  serial_port.write( [0, 0, 0, 0, SELECT_VFO] )
  
def sel_vfo_b(serial_port):
  serial_port.write( [0, 0, 0, 1, SELECT_VFO] )

def vfo_to_mem(serial_port, memory):
  serial_port.write( [0, 0, 0, memory, VFO_TO_MEM] )

def split_on(serial_port):
  serial_port.write( [0, 0, 0, 1, SPLIT] )

def split_off(serial_port):
  serial_port.write( [0, 0, 0, 0, SPLIT] )

def read_memory_from_dump(str_rx_data, base_address):
  vfo_a=(65536*ord(str_rx_data[base_address+1]))  + (256*ord(str_rx_data[base_address+2]))  + ord(str_rx_data[base_address+3]) 
  vfo_b=(65536*ord(str_rx_data[base_address+10])) + (256*ord(str_rx_data[base_address+11])) + ord(str_rx_data[base_address+12]) 
  vfo_a = vfo_a * 10
  vfo_b = vfo_b * 10
  mode_a="NULL"
  mode_b="NULL"
  if (ord(str_rx_data[base_address+6])  == 0):
    mode_a="LSB"
  if (ord(str_rx_data[base_address+6])  == 1):
    mode_a="USB"
  if (ord(str_rx_data[base_address+6])  == 2):
    mode_a="CW"
  if (ord(str_rx_data[base_address+6])  == 3):
    mode_a="AM"
  if (ord(str_rx_data[base_address+6])  == 4):
    mode_a="FM"
  if (ord(str_rx_data[base_address+15]) == 0):
    mode_b="LSB"
  if (ord(str_rx_data[base_address+15]) == 1):
    mode_b="USB"
  if (ord(str_rx_data[base_address+15]) == 2):
    mode_b="CW"
  if (ord(str_rx_data[base_address+15]) == 3):
    mode_b="AM"
  if (ord(str_rx_data[base_address+15]) == 4):
    mode_b="FM"
  #print("VFO A: %d %s" % (vfo_a,mode_a))
  #print("VFO B: %d %s" % (vfo_b,mode_b))
  #print (ord(rx_data[base_address+6]))
  return vfo_a, mode_a, vfo_b, mode_b

def set_mode(mode):
  if   mode == "LSB": set_mode = LSB
  elif mode == "USB": set_mode = USB
  elif mode == "CW": set_mode = CW
  elif mode == "AM": set_mode = AM
  elif mode == "FM": set_mode = FM
  ser.write( [0, 0, 0, set_mode, SET_MODE] )

def vfo_to_mem(serial_port, memory):
    serial_port.write( [0, 0, 0, memory, VFO_TO_MEM] )



if ( (len(sys.argv) != 3) or ((sys.argv[1] != '-u') and (sys.argv[1] != '-d')) or (sys.argv[2].split('.')[1] != "xlsx") ):
  print("Yaesu FT-840 memory management tool")
  print("Usage: Download frequencies from spreadsheet to radio:  python3 ft840mem.py -d filename.xlsx");
  print("       Upload frequencies from radio to spreadsheet:    python3 ft840mem.py -u filename.xlsx");
  print("       Use upload option to generate initial spreadsheet, then edit as approriate and download")
  exit()

filename = sys.argv[2]
  
ser = serial.Serial('/dev/ttyUSB0',baudrate=4800,bytesize=8, parity='N', stopbits=2)  #TIMEOUT


#Upload memories from radio to speadsheet
if sys.argv[1] == '-u':
  ser.write(ACQ_ALL_DATA)
  rx_data=ser.read(1941)
  #print("\nSerial bytes received: %d " % sys.getsizeof(rx_data))
  rx_data_str=rx_data.decode('latin1')

  mem_wb = openpyxl.Workbook()
  mem_ws = mem_wb.active
  mem_ws.cell(1,1,"Memory")
  mem_ws.cell(1,2,"Freq A")
  mem_ws.cell(1,3,"Mode A")
  mem_ws.cell(1,4,"Freq B")
  mem_ws.cell(1,5,"Mode B")

  #Column 1 is memory numbers
  for mem_index in range (1,101):
    mem_ws.cell(mem_index+1,1,mem_index)

  vfo_a  = [0 for i in range (101)]
  mode_a = ["NULL" for i in range (101)]
  vfo_b  = [0 for i in range (101)]
  mode_b = ["NULL" for i in range (101)]

  for mem_index in range (1,101):
    vfo_a[mem_index], mode_a[mem_index], vfo_b[mem_index], mode_b[mem_index] = read_memory_from_dump(rx_data_str,42+((mem_index-1)*19))
    print("Memory: %d: " % mem_index)
    print(vfo_a[mem_index], mode_a[mem_index], vfo_b[mem_index], mode_b[mem_index])
    mem_ws.cell(mem_index+1,2,float(vfo_a[mem_index]/1e6))
    mem_ws.cell(mem_index+1,3,mode_a[mem_index])
    mem_ws.cell(mem_index+1,4,float(vfo_b[mem_index]/1e6))
    mem_ws.cell(mem_index+1,5,mode_b[mem_index])

  mem_wb.save(filename)


#Download memories from spreadsheet to radio
if sys.argv[1] == '-d':
  if (os.path.isfile(filename) == False):
    print("File does not exist")
    exit()
  mem_wb_2 = openpyxl.load_workbook(filename)
  mem_ws_2 = mem_wb_2.active

  for mem_index in range (1,101):
    freq_a_float = mem_ws_2.cell(row=mem_index+1, column=2).value
    mode_a = mem_ws_2.cell(row=mem_index+1, column=3).value
    print("Memory: %d Freq A: %f Mode A: %s" % (mem_index, freq_a_float, mode_a) )
    sel_vfo_a(ser)
    set_freq(freq_a_float, ser)

    set_mode(mode_a)

    vfo_to_mem(ser, mem_index)

#split_on(ser)
#split_off(ser)
ser.close
exit()




  


