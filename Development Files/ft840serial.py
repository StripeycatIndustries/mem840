#Yaesu FT-840 serial control

import serial
import sys
import time


def read_memory(str_rx_data, base_address):
  vfo_a=(65536*ord(str_rx_data[base_address+1]))  + (256*ord(str_rx_data[base_address+2]))  + ord(str_rx_data[base_address+3]) 
  vfo_b=(65536*ord(str_rx_data[base_address+10])) + (256*ord(str_rx_data[base_address+11])) + ord(str_rx_data[base_address+12]) 
  vfo_a = vfo_a * 10
  vfo_b = vfo_b * 10
  mode_a="NULL"
  mode_b="NULL"
  if (ord(str_rx_data[base_address+6])  == 0):
    mode_a="LSB"
  if (ord(str_rx_data[base_address+6])  == 1):
    mode_a="USB"
  if (ord(str_rx_data[base_address+6])  == 2):
    mode_a="CW"
  if (ord(str_rx_data[base_address+6])  == 3):
    mode_a="AM"
  if (ord(str_rx_data[base_address+6])  == 4):
    mode_a="FM"
  if (ord(str_rx_data[base_address+15]) == 0):
    mode_b="LSB"
  if (ord(str_rx_data[base_address+15]) == 1):
    mode_b="USB"
  if (ord(str_rx_data[base_address+15]) == 2):
    mode_b="CW"
  if (ord(str_rx_data[base_address+15]) == 3):
    mode_b="AM"
  if (ord(str_rx_data[base_address+15]) == 4):
    mode_b="FM"

  #print("VFO A: %d %s" % (vfo_a,mode_a))
  #print("VFO B: %d %s" % (vfo_b,mode_b))

  #print (ord(rx_data[base_address+6]))

  return vfo_a, mode_a, vfo_b, mode_b


ACQ_ALL_DATA=[0x00,0x00,0x00,0x00,0x10]

ser = serial.Serial('/dev/ttyUSB0',baudrate=4800,bytesize=8, parity='N', stopbits=2)  #TIMEOUT
#tx_data=[0x00,0x50,0x42,0x01,0x0A] #SET FREQ VFO A 14.250

ser.write(ACQ_ALL_DATA)
rx_data=ser.read(1941)
rx_data_str=rx_data.decode('latin1')


print(rx_data_str)
for loop1 in range (0,1941):
  print("%d:  %s" % (loop1, hex(ord(rx_data_str[loop1]))) )

for loop1 in range (0,42):
  print ("%s " % hex(ord(rx_data_str[loop1]) ) ),
  print(" ")
vfo_a=(65536*ord(rx_data_str[6])) + (256*ord(rx_data_str[7])) + ord(rx_data_str[8])
print(" %d %d %d " % ( ord(rx_data_str[6]), ord(rx_data_str[7]), ord(rx_data_str[8])  ) )

print (vfo_a)

vfo_a, mode_a, vfo_b, mode_b = read_memory(rx_data_str,5)
print("# VFO A: %d %s" % (vfo_a,mode_a))
print("# VFO B: %d %s" % (vfo_b,mode_b))
read_memory(rx_data_str,23)
read_memory(rx_data_str,42)

for mem_index in range (0,100):
    print("Memory: %d: " % mem_index)
    vfo_a, mode_a, vfo_b, mode_b = read_memory(rx_data_str,42+(mem_index*19))
    print(vfo_a, mode_a, vfo_b, mode_b)
print("\nBytes received: %d " % sys.getsizeof(rx_data))
ser.close
print("Done")


  


